// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "InternetButton.h"

InternetButton b = InternetButton();

void setup() {
    b.begin();
    
    // playSong takes a string in the format
    // "NOTE,TYPE,NOTE,TYPE..."
    // Types are note types that define duration so 
    // 8 is a 1/8th note and 4 is a 1/4 note
    //b.playSong("C4,8,E4,8,G4,8,C5,8,G5,4");
    Particle.function("playnote", playnote);
    Particle.function("playdoublenote", playdoublenote);
}

void loop() {
   
}

int playnote(String command){
    
    b.playNote(command,8);
}

int playdoublenote(String command){
    
    b.playSong(command);
}