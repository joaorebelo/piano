package com.example.joorebelo.piano;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button buttonNoteC1, buttonNoteD1, buttonNoteE1, buttonNoteF1, buttonNoteG1, buttonNoteA1, buttonNoteB1, buttonNoteCs1, buttonNoteDs1, buttonNoteFs1, buttonNoteGs1, buttonNoteAs1;

    //region account
    // MARK: Particle Account Info
    private final String TAG="Joao";
    private final String PARTICLE_USERNAME = "joao.rebelo92@gmail.com";
    private final String PARTICLE_PASSWORD = "QWEparticle1";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "340036000f47363333343437";
    //endregion

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonNoteC1 = findViewById(R.id.buttonNoteC1);
        buttonNoteC1.setOnClickListener(this);
        buttonNoteD1 = findViewById(R.id.buttonNoteD1);
        buttonNoteD1.setOnClickListener(this);
        buttonNoteE1 = findViewById(R.id.buttonNoteE1);
        buttonNoteE1.setOnClickListener(this);
        buttonNoteF1 = findViewById(R.id.buttonNoteF1);
        buttonNoteF1.setOnClickListener(this);
        buttonNoteG1 = findViewById(R.id.buttonNoteG1);
        buttonNoteG1.setOnClickListener(this);
        buttonNoteA1 = findViewById(R.id.buttonNoteA1);
        buttonNoteA1.setOnClickListener(this);
        buttonNoteB1 = findViewById(R.id.buttonNoteB1);
        buttonNoteB1.setOnClickListener(this);
        buttonNoteCs1 = findViewById(R.id.buttonNoteCs1);
        buttonNoteCs1.setOnClickListener(this);
        buttonNoteDs1 = findViewById(R.id.buttonNoteDs1);
        buttonNoteDs1.setOnClickListener(this);
        buttonNoteFs1 = findViewById(R.id.buttonNoteFs1);
        buttonNoteFs1.setOnClickListener(this);
        buttonNoteGs1 = findViewById(R.id.buttonNoteGs1);
        buttonNoteGs1.setOnClickListener(this);
        buttonNoteAs1 = findViewById(R.id.buttonNoteAs1);
        buttonNoteAs1.setOnClickListener(this);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();


    }

    public void getDeviceFromCloud() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == buttonNoteC1.getId()){
            sendNote("C4");
            Log.d(TAG, "C4");
        }else if(v.getId() == buttonNoteD1.getId()){
            sendNote("D4");
            Log.d(TAG, "D4");
        }else if(v.getId() == buttonNoteE1.getId()){
            sendNote("E4");
            Log.d(TAG, "E4");
        }else if(v.getId() == buttonNoteF1.getId()){
            sendNote("F4");
            Log.d(TAG, "F4");
        }else if(v.getId() == buttonNoteG1.getId()){
            sendNote("G4");
            Log.d(TAG, "G4");
        }else if(v.getId() == buttonNoteA1.getId()){
            sendNote("A4");
            Log.d(TAG, "A4");
        }else if(v.getId() == buttonNoteB1.getId()){
            sendNote("B4");
            Log.d(TAG, "B4");
        }else if(v.getId() == buttonNoteCs1.getId()){
            sendDoubleNote("C4,8,D4,8");
            Log.d(TAG, "C4,8,D4,8");
        }else if(v.getId() == buttonNoteDs1.getId()){
            sendDoubleNote("D4,8,E4,8");
            Log.d(TAG, "D4,8,E4,8");
        }else if(v.getId() == buttonNoteFs1.getId()){
            sendDoubleNote("F4,8,G4,8");
            Log.d(TAG, "F4,8,G4,8");
        }else if(v.getId() == buttonNoteGs1.getId()){
            sendDoubleNote("G4,8,A4,8");
            Log.d(TAG, "G4,8,A4,8");
        }else if(v.getId() == buttonNoteAs1.getId()){
            sendDoubleNote("A4,8,B4,8");
            Log.d(TAG, "A4,8,B4,8");
        }
    }

    private void sendNote(String commandToSend){
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(commandToSend);

                try {
                    mDevice.callFunction("playnote", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Sent colors command to device.");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    private void sendDoubleNote(String commandToSend){
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(commandToSend);

                try {
                    mDevice.callFunction("playdoublenote", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Sent colors command to device.");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
}
